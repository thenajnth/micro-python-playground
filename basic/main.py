import time
import network

from machine import Pin, PWM

led = Pin(2, Pin.OUT)


# Flash light
for _ in range(5):
    led.value(not led.value())
    time.sleep(2)


led_pwm = PWM(led)
led_pwm.freq(500)

# Dim light
for i in range(5):
    led_pwm.duty(20*i)
    time.sleep(2)

sta_if = network.WLAN(network.STA_IF)

print('Nätverk status: {}'.format('På' if sta_if.isconnected() else 'av'))

if sta_if.isconnected():
    ip, mask, gw, dns = sta_if.ifconfig()
    print('IP-adress: {}'.format(ip))
    print('Nätverksmask: {}'.format(mask))
    print('Gateway: {}'.format(gw))
    print('DNS: {}'.format(dns))
