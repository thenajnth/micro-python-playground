import time

from machine import Pin, PWM

D1 = Pin(5, Pin.OUT)
D2 = Pin(4, Pin.OUT)
D3 = Pin(0, Pin.OUT)
D4 = Pin(2, Pin.OUT)
D5 = Pin(14, Pin.OUT)
D6 = Pin(12, Pin.OUT)
D7 = Pin(13, Pin.OUT)
D8 = Pin(15, Pin.OUT)

LED1 = PWM(D1)
LED2 = PWM(D2)
LED3 = PWM(D3)
LED4 = PWM(D4)
LED5 = PWM(D5)
LED6 = PWM(D6)
LED7 = PWM(D7)
LED8 = PWM(D8)

ALL_LEDS = [LED1, LED2, LED3, LED4, LED5, LED6, LED7, LED8]
LED1.freq(1000)


def _fade_leds(leds, delay, duty_start, duty_stop, step):
    for i in range(duty_start, duty_stop, step):
        for led in leds:
            led.duty(i)
        time.sleep(delay)


def fade_up_leds(leds, delay=0.1, duty_start=0, duty_stop=40, step=1):
    _fade_leds(leds, delay, duty_start, duty_stop, step)


def fade_down_leds(leds, delay=0.1, duty_start=40, duty_stop=0, step=2):
    _fade_leds(leds, delay, duty_start, duty_stop, -step)


def clear_leds(leds):
    for led in leds:
        led.duty(0)


# Flash one pin at a time
def walk_leds(leds, delay=0.2):
    for led in leds:
        clear_leds(leds)
        led.duty(100)
        time.sleep(delay)


if __name__=='__main__':
    while True:
        walk_leds(ALL_LEDS, delay=0.1)
        walk_leds(ALL_LEDS[::-1], delay=0.1)
        clear_leds(ALL_LEDS)

        # Fade up
        fade_up_leds(ALL_LEDS)

        # Fade down
        fade_down_leds(ALL_LEDS)
