import time
# import network

from machine import Pin, PWM

# led = Pin(2, Pin.OUT)

# for _ in range(5):
#     led.value(not led.value())
#     time.sleep(2)

# sta_if = network.WLAN(network.STA_IF)

# print('Nätverk status: {}'.format('På' if sta_if.isconnected() else 'av'))

# if sta_if.isconnected():
#     ip, mask, gw, dns = sta_if.ifconfig()
#     print('IP-adress: {}'.format(ip))
#     print('Nätverksmask: {}'.format(mask))
#     print('Gateway: {}'.format(gw))
#     print('DNS: {}'.format(dns))


# Buzzer
pin = Pin(12, Pin.OUT)
buzzer = PWM(pin)
buzzer.duty(50)

buzzer.freq(1000)
time.sleep(1)
buzzer.freq(500)
time.sleep(1)
buzzer.freq(120)

time.sleep(1)
buzzer.duty(0)
buzzer.deinit()
