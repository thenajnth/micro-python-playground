import dht

from time import sleep
from machine import Pin

D5 = Pin(14)

sensor = dht.DHT11(D5)

while True:
    sensor.measure()
    print('\n\nTemperatur: {}'.format(sensor.temperature()))
    sleep(5)
