from machine import Pin, PWM
from time import sleep


SERVO_PIN = PWM(Pin(5), freq=50)


def turn_servo(servo, value):
    """Turns servo

    :param servo: A PWM-object
    :param value: A value between 0-100.
    """
    value = min(value, 100)  # Never exceed 100
    value = max(value, 0)  # Never lower than 0
    print('Turn servo to {}.'.format(value))

    value = value + 25
    servo.duty(value)


if __name__ == '__main__':
    i = 0

    while True:
        turn_servo(SERVO_PIN, i)
        i = (i + 5) % 100
        sleep(0.4)
